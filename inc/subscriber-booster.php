<?php
/**
 * Functions to enable the subscriber booster on easypress theme
 *
 *
 * @package easypress
 */

/**
* Including the necessary actions to handle UI Ajax Calls
*/
add_action( 'wp_enqueue_scripts', 'subscriber_booster_submit_scripts' );  
add_action( 'wp_ajax_ajax-subscriberBooster', 'subscriber_booster_handle_post' );
add_action( 'wp_ajax_nopriv_ajax-subscriberBooster', 'subscriber_booster_handle_post' );


/**
* This function includes all necessary java scripts 
*/
function subscriber_booster_submit_scripts() {
  
    wp_enqueue_script( 'subscriber-booster', get_template_directory_uri() . '/inc/js/subscriber-booster.js', array( 'jquery' ));	
    wp_localize_script( 'subscriber-booster', 'SB_Ajax', array(
        'ajaxurl'       => admin_url( 'admin-ajax.php' ),
        'nextNonce'     => wp_create_nonce( 'myajax-next-nonce' ))
    );
	
}

/**
* This functions will handle the UI form submission
*/
function subscriber_booster_handle_post() {
	// check nonce
	$nonce = $_POST['nextNonce']; 	
	if ( ! wp_verify_nonce( $nonce, 'myajax-next-nonce' ) )
		die ( 'Sorry! We could not proccess your request.');
		
    /*		
	$_POST["key"] = of_get_option( 'newsletter_ca_mailchimp_api_key' );

	// generate the response
	$response = json_encode( $_POST );
 
	// response output
	header( "Content-Type: application/json" );
	echo $response;
    */

	$apiKey = of_get_option( 'subscriber_booster_mailchimp_api_key' );
	$listId = of_get_option( 'subscriber_booster_list_id' );

	$double_optin=false;
	$send_welcome=false;

	$email_type = 'html';
	$email = $_POST['email'];

	//TO-DO: Stripe the actual datacenter from the mailchimp api key
	$datacenter = "us7";

	$submit_url = "http://".$datacenter.".api.mailchimp.com/1.3/?method=listSubscribe";

	$data = array(
	    'email_address'=>$email,
	    'apikey'=>$apiKey,
	    'id' => $listId,
	    'double_optin' => $double_optin,
	    'send_welcome' => $send_welcome,
	    'email_type' => $email_type
	);

	$payload = json_encode($data);
 
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $submit_url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, urlencode($payload));
 
	$result = curl_exec($ch);

	curl_close ($ch);

    header( "Content-Type: application/json" );
	echo $result;

	//$data = json_decode($result);

	// response output
	//header( "Content-Type: application/json" );

	//if ($data->error){
	//    echo $result;
	//} else {
	//    echo 'You have successfuly subscribed. Welcome!';
	//}

	// IMPORTANT: don't forget to "exit"
	exit;
	
}