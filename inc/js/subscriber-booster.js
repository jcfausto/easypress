/**
 * Easypress Subscriber Booster
 *
 * Contains handlers to enable the subscriber booster funcionality.
 */
(function($) {
  $(document).ready(function(){

		$('#subscriber-booster-button').click(function(){
		var subscriber = $('#subscriber-booster-subscriber').attr('value')

		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		var validEmail = pattern.test(subscriber);

		if (!validEmail) {
			$('#subscriber-booster-result').css('display', '');			
			$('#subscriber-booster-result').html('<strong>Attention!</strong> Invalid Email Address.');
		}

		if ((subscriber != '') && (validEmail)) {

			$.post(
				SB_Ajax.ajaxurl,
				{
					// wp ajax action
					action : 'ajax-subscriberBooster',
					
					// vars
					email : subscriber,
				 
					// send the nonce along with the request
					nextNonce : SB_Ajax.nextNonce
				},
				function( response ) {		

					if (response === true) {
						$('#subscriber-booster-form').css('display', 'none');	
						$('#subscriber-booster-result').css('display', '');		
						$('#subscriber-booster-result').html('<strong>Welcome!</strong> You have successfuly subscribed. Enjoy the content!');
					} else {
						$('#subscriber-booster-result').css('display', '');			
						$('#subscriber-booster-result').html('<strong>Attention!</strong> ' + response['error']);
					}

				}
			);
		}

		return false;

	  });	
		    	
	});
})(jQuery);