<?php

/**
 * Social  Widget
 * easypress Theme
 */
class easypress_social_widget extends WP_Widget
{
	 function easypress_social_widget(){

        $widget_ops = array('classname' => 'easypress-social','description' => __( "Easypress Social Widget" ,'easypress') );
		    parent::__construct('easypress-social', __('Easypress Social Widget','easypress'), $widget_ops);
    }

    function widget($args , $instance) {
    	extract($args);
        $title = ($instance['title']) ? $instance['title'] : __('Follow us' , 'easypress');

      echo $before_widget;
      echo $before_title;
      echo $title;
      echo $after_title;

		/**
		 * Widget Content
		 */
    ?>

    <!-- social icons -->
    <div class="social-icons sticky-sidebar-social">


    <?php easypress_social(); ?>


    </div><!-- end social icons -->


		<?php

		echo $after_widget;
    }


    function form($instance) {
      if(!isset($instance['title'])) $instance['title'] = __('Follow us' , 'easypress');
    ?>

      <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title ','easypress') ?></label>

      <input type="text" value="<?php echo esc_attr($instance['title']); ?>"
                          name="<?php echo $this->get_field_name('title'); ?>"
                          id="<?php $this->get_field_id('title'); ?>"
                          class="widefat" />
      </p>

    	<?php
    }

}

?>