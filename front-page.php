<?php

    if ( get_option( 'show_on_front' ) == 'posts' ) {
        get_template_part( 'index' );
    } elseif ( 'page' == get_option( 'show_on_front' ) ) {

get_header(); ?>
</div>

<div id="content" class="site-content container-full">
	<div id="header" class="content-area col-sm-12 col-md-12">
		<main id="main" class="site-main" role="main">
		   <div class="header-intro">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 col-lg-offset-2 centered">
                        	<?php
                        	  /* Checking if the image has been set. */
                        	  if ( of_get_option( 'im-image', 'Hello!' ) != '') { ?>
                        		<img width="200" height="200" src="<?php echo of_get_option( 'im-image', 'Hello!' ); ?>" class="attachment-post-thumbnail wp-post-image" alt="user">
                    		  <?php } ?>	
                    		<h1><?php echo of_get_option( 'im-greeting', 'Hello!' ); ?></h1>
           					<p><?php echo of_get_option( 'im-description', 'Hello!' ); ?></p>
                          </div>
					</div>
				</div>

				<!-- Header Social Bar - Optional -->
				<?php if ( of_get_option( 'im-social-bar', 0 ) == 1 ) { easypress_get_im_social_bar(); } ?>
				<!-- End of Header Social Bar - Optional -->

		   </div><!-- #header-intro -->


		   <?php
	    		/* Checking if the call to action button is enabled. */
	    	  	if ( of_get_option( 'im-ca-visit-my-blog', 0 ) == 1) { ?>
					<div id="ca-visit-my-blog-button">
					   <div class="container">
							<div class="col-sm-12 centered">
								<a class="btn btn-lg cfa-button" href="<?php echo of_get_option( 'im-ca-visit-my-blog-url', '/blog/' ); ?>"><?php echo of_get_option( 'im-ca-visit-my-blog-label', 'Visit my blog!' ); ?></a>
							 </div>
						</div>
					</div><!-- #ca-visit-my-blog-button -->
			<?php } ?>
		   

		</main><!-- #main -->
	</div><!-- #header -->

	<!-- Services section - Optional -->
	<?php if ( of_get_option( 'services-active', 0 ) == 1 ) { easypress_get_services_section(); } ?>
	<!-- End of Services section - Optional -->


	<!-- Skills section - Optional -->
	<?php if ( of_get_option( 'skills-active', 0 ) == 1 ) { easypress_get_skills_section(); } ?>
	<!-- End of Skills section - Optional -->

    <!-- Unfinished section - TO-DO
	<div id="portfolio-section" class="content-area col-sm-12 col-md-12">
		<div id="services-container" class="container">
			<h2 class="centered front-page-section-header-intro">LATEST WORKS</h2>

			<div id="latest-works">
	        	<ul id="portfolio">
				  <li class="item webdesign"><a href="#"><img src="http://localhost:8080/wp-content/uploads/2014/10/portfolio1.jpg" alt="wordpress theme"></a></li>
				  <li class="item illustration"><a href="#"><img src="http://localhost:8080/wp-content/uploads/2014/10/portfolio2.jpg" alt="able baker space floating monkeys"></a></li>
				  <li class="item appicon"><a href="#"><img src="http://localhost:8080/wp-content/uploads/2014/10/portfolio3.jpg" alt="contacts iphone app icon"></a></li>
				</ul><!-- @end #portfolio 
			</div>
		</div>
	</div> 


	<div id="contact-section" class="content-area col-sm-12 col-md-12">
		<div id="services-container" class="container">
			<h2 class="centered front-page-section-header-intro">GET IN TOUCH</h2>


				contac form here!


		</div>
	</div>
	-->

<?php
	get_footer();
}
?>