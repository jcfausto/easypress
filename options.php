<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 *
 */

function optionsframework_option_name() {

        // This gets the theme name from the stylesheet
        $themename = wp_get_theme();
        $themename = preg_replace("/\W/", "_", strtolower($themename) );

        $optionsframework_settings = get_option( 'optionsframework' );
        $optionsframework_settings['id'] = $themename;
        update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 */

function optionsframework_options() {

		// Layout options
		$site_layout = array('pull-left' => __('Right Sidebar', 'easypress'),'pull-right' => __('Left Sidebar', 'easypress'));

       // Test data
        $test_array = array(
                'one' => __('One', 'options_framework_theme'),
                'two' => __('Two', 'options_framework_theme'),
                'three' => __('Three', 'options_framework_theme'),
                'four' => __('Four', 'options_framework_theme'),
                'five' => __('Five', 'options_framework_theme')
        );

        // Multicheck Array
        $multicheck_array = array(
                'one' => __('French Toast', 'options_framework_theme'),
                'two' => __('Pancake', 'options_framework_theme'),
                'three' => __('Omelette', 'options_framework_theme'),
                'four' => __('Crepe', 'options_framework_theme'),
                'five' => __('Waffle', 'options_framework_theme')
        );

        // Multicheck Defaults
        $multicheck_defaults = array(
                'one' => '1',
                'five' => '1'
        );

        // Typography Defaults
        $typography_defaults = array(
                'size' => '14px',
                'face' => 'Open Sans',
                'style' => 'normal',
                'color' => '#6B6B6B' );

        // Typography Options
        $typography_options = array(
                'sizes' => array( '6','10','12','14','15','16','18','20','24','28','32','36','42','48' ),
                'faces' => array(
													'arial'     => 'Arial',
													'verdana'   => 'Verdana, Geneva',
													'trebuchet' => 'Trebuchet',
													'georgia'   => 'Georgia',
													'times'     => 'Times New Roman',
													'tahoma'    => 'Tahoma, Geneva',
													'Open Sans' 	=> 'Open Sans',
													'palatino'  => 'Palatino',
													'helvetica' => 'Helvetica',
													'Helvetica Neue' => 'Helvetica Neue'
				),
                'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
                'color' => true
        );

        // $radio = array('0' => __('No', 'easypress'),'1' => __('Yes', 'easypress'));

     // Pull all the categories into an array
        $options_categories = array();
        $options_categories_obj = get_categories();
        foreach ($options_categories_obj as $category) {
                $options_categories[$category->cat_ID] = $category->cat_name;
        }

        // Pull all tags into an array
        $options_tags = array();
        $options_tags_obj = get_tags();
        foreach ( $options_tags_obj as $tag ) {
                $options_tags[$tag->term_id] = $tag->name;
        }


        // Pull all the pages into an array
        $options_pages = array();
        $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
        $options_pages[''] = 'Select a page:';
        foreach ($options_pages_obj as $page) {
                $options_pages[$page->ID] = $page->post_title;
        }

        // If using image radio buttons, define a directory path
        $imagepath =  get_template_directory_uri() . '/images/';


		// fixed or scroll position
		$fixed_scroll = array('scroll' => 'Scroll', 'fixed' => 'Fixed');

		$options = array();

		$options[] = array( 'name' => __('Main', 'easypress'),
							'type' => 'heading');

		/* Disabled for a while

		$options[] = array( 'name' => __('Do You want to display image slider on the Home Page?','easypress'),
							'desc' => __('Check if you want to enable slider', 'easypress'),
							'id' => 'easypress_slider_checkbox',
							'std' => 0,
							'type' => 'checkbox');


		$options[] = array( 'name' => __('Slider Category', 'easypress'),
							'desc' => __('Select a category for the featured post slider', 'easypress'),
							'id' => 'easypress_slide_categories',
							'type' => 'select',
							'class' => 'hidden',
							'options' => $options_categories);

		$options[] = array( 'name' => __('Number of slide items', 'easypress'),
							'desc' => __('Enter the number of slide items', 'easypress'),
							'id' => 'easypress_slide_number',
							'std' => '3',
							'class' => 'hidden',
							'type' => 'text');
		*/

		$options[] = array( 'name' => __('Website Layout Options', 'easypress'),
							'desc' => __('Choose between Left and Right sidebar options to be used as default', 'easypress'),
							'id' => 'site_layout',
							'std' => 'pull-left',
							'type' => 'select',
							'class' => 'mini',
							'options' => $site_layout);
		

		$options[] = array( 'name' => __('Element color', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'element_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Element color on hover', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'element_color_hover',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Custom Favicon', 'easypress'),
							'desc' => __('Upload a 32px x 32px PNG/GIF image that will represent your websites favicon', 'easypress'),
							'id' => 'custom_favicon',
							'std' => '',
							'type' => 'upload');

	    /* Intro section */
		$options[] = array( 'name' => __('Intro', 'easypress'),
							'type' => 'heading');

		$options[] = array( 'name' => __('Image', 'easypress'),
							'desc' => __('Recommended image size: 200px x 200px.', 'easypress'),
							'id' => 'im-image',
							'std' => '',
							'type' => 'upload');

		$options[] = array( 'name' => __('Greeting', 'easypress'),
							'desc' => __('Type a short greeting message.', 'easypress'),
							'id' => 'im-greeting',
							'std' => 'Hello!',
							'type' => 'text');

		$options[] = array( 'name' => __('Description', 'easypress'),
							'desc' => __('Type a description here.', 'easypress'),
							'id' => 'im-description',
							'std' => 'Hello everybody. I’m Myself, A smart person using a smart theme. Feel free to keep in touch.',
							'type' => 'editor');

		$options[] = array( 'name' => __('Add a social bar in the header','easypress'),
							'desc' => __('Check to add social bar in your header intro.', 'easypress'),
							'id' => 'im-social-bar',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Enable call to action button','easypress'),
							'desc' => __('Check to enable a call to action button below your description.', 'easypress'),
							'id' => 'im-ca-visit-my-blog',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Call to action button label', 'easypress'),
							'desc' => __('Type a label for your button.', 'easypress'),
							'id' => 'im-ca-visit-my-blog-label',
							'std' => 'Visit my blog!',
							'type' => 'text');

		$options[] = array( 'name' => __('Call to action target URL', 'easypress'),
							'desc' => __('Type a target URL.', 'easypress'),
							'id' => 'im-ca-visit-my-blog-url',
							'std' => '/blog/',
							'type' => 'text');
		/* End Intro section */

		
		/* Services section */
		$options[] = array( 'name' => __('Services', 'easypress'),
							'type' => 'heading');

		$options[] = array( 'name' => __('Enable services section','easypress'),
							'desc' => __('Check to enable the services section on the front page.', 'easypress'),
							'id' => 'services-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Title', 'easypress'),
							'desc' => __('Type a title for your services section.', 'easypress'),
							'id' => 'services-title',
							'std' => 'Services',
							'type' => 'text');

		$options[] = array( 'name' => __('SERVICE ONE: ACTIVATE','easypress'),
							'desc' => __('Check to show the service one.', 'easypress'),
							'id' => 'service-one-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Service one - Title', 'easypress'),
							'desc' => __('Type a title for your first service.', 'easypress'),
							'id' => 'service-one-title',
							'std' => 'Web Development',
							'type' => 'text');

		$options[] = array( 'name' => __('Service one - Subtitle', 'easypress'),
							'desc' => __('Type a subtitle for your first service.', 'easypress'),
							'id' => 'service-one-subtitle',
							'std' => 'Web Apss, HTML5, CSS3, JQuery, Rails',
							'type' => 'text');

		$options[] = array( 'name' => __('Service one - Font awessome icon code', 'easypress'),
							'desc' => __('Type a font awessome icon code here to identify your service.', 'easypress'),
							'id' => 'service-one-fa-icon',
							'std' => 'fa-laptop',
							'type' => 'text');

		$options[] = array( 'name' => __('SERVICE TWO: ACTIVATE','easypress'),
							'desc' => __('Check to show the second service.', 'easypress'),
							'id' => 'service-two-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Service two - Title', 'easypress'),
							'desc' => __('Type a title for your second service.', 'easypress'),
							'id' => 'service-two-title',
							'std' => 'Mobile Development',
							'type' => 'text');

		$options[] = array( 'name' => __('Service two - Subtitle', 'easypress'),
							'desc' => __('Type a subtitle for your sercond service.', 'easypress'),
							'id' => 'service-two-subtitle',
							'std' => 'iOS Apss and Android Apps',
							'type' => 'text');

		$options[] = array( 'name' => __('Service two - Font awessome icon code', 'easypress'),
							'desc' => __('Type a font awessome icon code here to identify your service.', 'easypress'),
							'id' => 'service-two-fa-icon',
							'std' => 'fa-mobile',
							'type' => 'text');

		$options[] = array( 'name' => __('SERVICE THREE: ACTIVATE','easypress'),
							'desc' => __('Check to show the third service.', 'easypress'),
							'id' => 'service-three-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Service three - Title', 'easypress'),
							'desc' => __('Type a title for your third service.', 'easypress'),
							'id' => 'service-three-title',
							'std' => 'Consulting',
							'type' => 'text');

		$options[] = array( 'name' => __('Service three - Subtitle', 'easypress'),
							'desc' => __('Type a subtitle for your third service.', 'easypress'),
							'id' => 'service-three-subtitle',
							'std' => 'Agile transformation, Team building',
							'type' => 'text');

		$options[] = array( 'name' => __('Service three - Font awessome icon code', 'easypress'),
							'desc' => __('Type a font awessome icon code here to identify your service.', 'easypress'),
							'id' => 'service-three-fa-icon',
							'std' => 'fa-heart-o',
							'type' => 'text');

		/* End of Services section */

		/* Skills section */

		$options[] = array( 'name' => __('Skills', 'easypress'),
							'type' => 'heading');

		$options[] = array( 'name' => __('Enable skills section','easypress'),
							'desc' => __('Check to enable the skills section on the front page.', 'easypress'),
							'id' => 'skills-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Skill title', 'easypress'),
							'desc' => __('Type a title for your skills section.', 'easypress'),
							'id' => 'skill-section-title',
							'std' => 'SOME SKILLS',
							'type' => 'text');

		$options[] = array( 'name' => __('SKILL ONE: ACTIVATE','easypress'),
							'desc' => __('Check to show the first skill.', 'easypress'),
							'id' => 'skill-one-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Skill one - Text', 'easypress'),
							'desc' => __('Type a text for this skill.', 'easypress'),
							'id' => 'skill-one-text',
							'std' => 'Wordpress development',
							'type' => 'text');

		$options[] = array( 'name' => __('Skill one - Percentage', 'easypress'),
							'desc' => __('Type the percentage for this skill. (Absolute number. i.e. 50)', 'easypress'),
							'id' => 'skill-one-percentage',
							'std' => '50',
							'type' => 'text');

		$options[] = array( 'name' => __('SKILL TWO: ACTIVATE','easypress'),
							'desc' => __('Check to show the second skill.', 'easypress'),
							'id' => 'skill-two-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Skill two - Text', 'easypress'),
							'desc' => __('Type a text for this skill.', 'easypress'),
							'id' => 'skill-two-text',
							'std' => 'Web development',
							'type' => 'text');

		$options[] = array( 'name' => __('Skill two - Percentage', 'easypress'),
							'desc' => __('Type the percentage for this skill. (Absolute number. i.e. 50)', 'easypress'),
							'id' => 'skill-two-percentage',
							'std' => '50',
							'type' => 'text');

		$options[] = array( 'name' => __('SKILL THREE: ACTIVATE','easypress'),
							'desc' => __('Check to show the third skill.', 'easypress'),
							'id' => 'skill-three-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Skill three - Text', 'easypress'),
							'desc' => __('Type a text for this skill.', 'easypress'),
							'id' => 'skill-three-text',
							'std' => 'Database development',
							'type' => 'text');

		$options[] = array( 'name' => __('Skill three - Percentage', 'easypress'),
							'desc' => __('Type the percentage for this skill. (Absolute number. i.e. 50)', 'easypress'),
							'id' => 'skill-three-percentage',
							'std' => '50',
							'type' => 'text');

		$options[] = array( 'name' => __('SKILL FOUR: ACTIVATE','easypress'),
							'desc' => __('Check to show the fourth skill.', 'easypress'),
							'id' => 'skill-four-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Skill four - Text', 'easypress'),
							'desc' => __('Type a text for this skill.', 'easypress'),
							'id' => 'skill-four-text',
							'std' => 'Mobile development',
							'type' => 'text');

		$options[] = array( 'name' => __('Skill four - Percentage', 'easypress'),
							'desc' => __('Type the percentage for this skill. (Absolute number. i.e. 50)', 'easypress'),
							'id' => 'skill-four-percentage',
							'std' => '50',
							'type' => 'text');

		$options[] = array( 'name' => __('SKILL FIVE: ACTIVATE','easypress'),
							'desc' => __('Check to show the fifth skill.', 'easypress'),
							'id' => 'skill-five-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Skill five - Text', 'easypress'),
							'desc' => __('Type a text for this skill.', 'easypress'),
							'id' => 'skill-five-text',
							'std' => 'Agile consulting',
							'type' => 'text');

		$options[] = array( 'name' => __('Skill five - Percentage', 'easypress'),
							'desc' => __('Type the percentage for this skill. (Absolute number. i.e. 50)', 'easypress'),
							'id' => 'skill-five-percentage',
							'std' => '50',
							'type' => 'text');

		$options[] = array( 'name' => __('Donwload my CV Buttom','easypress'),
							'desc' => __('Check to show the buttom above the skills bars.', 'easypress'),
							'id' => 'skills-download-cv-active',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Donwload my CV Buttom - Title', 'easypress'),
							'desc' => __('Type the title for the download buttom.', 'easypress'),
							'id' => 'skill-download-cv-title',
							'std' => 'Download my CV',
							'type' => 'text');

		$options[] = array( 'name' => __('Donwload my CV Buttom - Link', 'easypress'),
							'desc' => __('URL of the CV for download.', 'easypress'),
							'id' => 'skill-download-cv-url',
							'std' => '',
							'type' => 'text');

		/* End of Skills section */


		 /* book review section */

		$options[] = array( 'name' => __('BookReview', 'easypress'),
							'type' => 'heading');

		$options[] = array( 'name' => __('Enable book review top section','easypress'),
							'desc' => __('Check if you want to show the book review section.', 'easypress'),
							'id' => 'book-review',
							'std' => 0,
							'type' => 'checkbox');

		$options[] = array( 'name' => __('Book Title', 'easypress'),
							'desc' => __('Type the book title.', 'easypress'),
							'id' => 'book-review-title',
							'std' => '',
							'type' => 'text');

		$options[] = array( 'name' => __('Book Cover', 'easypress'),
							'desc' => __('Recommended image size: 110px x 190px.', 'easypress'),
							'id' => 'book-review-cover',
							'std' => '',
							'type' => 'upload');

		$options[] = array( 'name' => __('Book Review', 'easypress'),
							'desc' => __('Write a short review here.', 'easypress'),
							'id' => 'book-review-short-review',
							'std' => '',
							'type' => 'editor');

		$options[] = array( 'name' => __('Post url that contains the full review', 'easypress'),
							'desc' => __('Type a target URL.', 'easypress'),
							'id' => 'book-review-full-review',
							'std' => '',
							'type' => 'text');

		$options[] = array( 'name' => __('Affiliated link to buy the book', 'easypress'),
							'desc' => __('Type an affiliated link or book url in your preferred online store.', 'easypress'),
							'id' => 'book-review-buy-this-book',
							'std' => '',
							'type' => 'text');

		/* end of book review section */

		/* Easypress Subscriber Booster */

		$options[] = array( 'name' => __('Subscriber Booster', 'easypress'),
							'type' => 'heading');

		$options[] = array( 'name' => __('Mailchimp API Key', 'easypress'),
							'desc' => __('Mailchimp API Key', 'easypress'),
							'id' => 'subscriber_booster_mailchimp_api_key',
							'std' => '',
							'type' => 'text');	

		$options[] = array( 'name' => __('Mailchimp List ID', 'easypress'),
							'desc' => __('Mailchimp List ID', 'easypress'),
							'id' => 'subscriber_booster_list_id',
							'std' => '',
							'type' => 'text');	

		$options[] = array( 'name' => __('Text', 'easypress'),
							'desc' => __('Enter the text', 'easypress'),
							'id' => 'subscriber_booster_text',
							'std' => '',
							'type' => 'textarea');

		$options[] = array( 'name' => __('Button Title', 'easypress'),
							'desc' => __('Enter the title', 'easypress'),
							'id' => 'subscriber_booster_button_title',
							'std' => '',
							'type' => 'text');

		/* end of Easypress Subscriber Booster */

		$options[] = array( 'name' => __('Action Button', 'easypress'),
							'type' => 'heading');

		$options[] = array( 'name' => __('Call For Action Text', 'easypress'),
							'desc' => __('Enter the text for call for action section', 'easypress'),
							'id' => 'w2f_cfa_text',
							'std' => '',
							'type' => 'textarea');

		$options[] = array( 'name' => __('Call For Action Button Title', 'easypress'),
							'desc' => __('Enter the title for Call For Action button', 'easypress'),
							'id' => 'w2f_cfa_button',
							'std' => '',
							'type' => 'text');

		$options[] = array( 'name' => __('CFA button link', 'easypress'),
							'desc' => __('Enter the link for Call For Action button', 'easypress'),
							'id' => 'w2f_cfa_link',
							'std' => '',
							'type' => 'text');

		$options[] = array( 'name' => __('Call For Action Text Color', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'cfa_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Call For Action Background Color', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'cfa_bg_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Call For Action Button Border Color', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'cfa_btn_color',
							'std' => '',
							'type' => 'color');
		$options[] = array( 'name' => __('Call For Action Button Text Color', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'cfa_btn_txt_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Typography', 'easypress'),
							'type' => 'heading');	

		$options[] = array( 'name' => __('Main Body Text', 'easypress'),
							'desc' => __('Used in P tags', 'easypress'),
							'id' => 'main_body_typography',
							'std' => $typography_defaults,
							'type' => 'typography',
							'options' => $typography_options );

		$options[] = array( 'name' => __('Heading Color', 'easypress'),
							'desc' => __('Color for all headings (h1-h6)', 'easypress'),
							'id' => 'heading_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Link Color', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'link_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Link:hover Color', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'link_hover_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Header', 'easypress'),
							'type' => 'heading');

		$options[] = array( 'name' => __('Top nav background color', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'nav_bg_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Top nav item color', 'easypress'),
							'desc' => __('Link color', 'easypress'),
							'id' => 'nav_link_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Top nav item hover color', 'easypress'),
							'desc' => __('Link:hover color', 'easypress'),
							'id' => 'nav_item_hover_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Top nav dropdown background color', 'easypress'),
							'desc' => __('Background of dropdown item hover color', 'easypress'),
							'id' => 'nav_dropdown_bg',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Top nav dropdown item color', 'easypress'),
							'desc' => __('Dropdown item color', 'easypress'),
							'id' => 'nav_dropdown_item',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Top nav dropdown item hover color', 'easypress'),
							'desc' => __('Dropdown item hover color', 'easypress'),
							'id' => 'nav_dropdown_item_hover',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Top nav dropdown item background hover color', 'easypress'),
							'desc' => __('Background of dropdown item hover color', 'easypress'),
							'id' => 'nav_dropdown_bg_hover',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Footer', 'easypress'),
							'type' => 'heading');

		$options[] = array( 'name' => __('Footer widget area background color', 'easypress'),
							'id' => 'footer_widget_bg_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Footer background color', 'easypress'),
							'id' => 'footer_bg_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Footer text color', 'easypress'),
							'id' => 'footer_text_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Footer link color', 'easypress'),
							'id' => 'footer_link_color',
							'std' => '',
							'type' => 'color');

		$options[] = array(	'name' => __('Footer information', 'easypress'),
        			'desc' => __('Copyright text in footer', 'easypress'),
        			'id' => 'custom_footer_text',
        			'std' => '<a href="' . esc_url( home_url( '/' ) ) . '" title="' . esc_attr( get_bloginfo( 'name', 'display' ) ) . '" >' . get_bloginfo( 'name', 'display' ) . '</a>  All rights reserved.',
        			'type' => 'textarea');

		$options[] = array( 'name' => __('Social', 'easypress'),
							'type' => 'heading');

		$options[] = array( 'name' => __('Social icon color', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'social_color',
							'std' => '',
							'type' => 'color');

		$options[] = array( 'name' => __('Footer social icon color', 'easypress'),
							'desc' => __('Default used if no color is selected', 'easypress'),
							'id' => 'social_footer_color',
							'std' => '',
							'type' => 'color');

		$options[] = array(	'name' => __('Add full URL for your social network profiles', 'easypress'),
        			'desc' => __('Facebook', 'easypress'),
        			'id' => 'social_facebook',
        			'std' => '',
        			'class' => 'mini',
        			'type' => 'text');

		$options[] = array(	'id' => 'social_twitter',
							'desc' => __('Twitter', 'easypress'),
        			'std' => '',
        			'class' => 'mini',
        			'type' => 'text');

		$options[] = array(	'id' => 'social_googleplus',
							'desc' => __('Google+', 'easypress'),
        			'std' => '',
        			'class' => 'mini',
        			'type' => 'text');

		$options[] = array(	'id' => 'social_youtube',
							'desc' => __('Youtube', 'easypress'),
        			'std' => '',
        			'class' => 'mini',
        			'type' => 'text');

		$options[] = array(	'id' => 'social_vimeo',
							'desc' => __('Vimeo', 'easypress'),
        			'std' => '',
        			'class' => 'mini',
        			'type' => 'text');

		$options[] = array(	'id' => 'social_linkedin',
							'desc' => __('LinkedIn', 'easypress'),
        			'std' => '',
        			'class' => 'mini',
        			'type' => 'text');

		$options[] = array(	'id' => 'social_pinterest',
							'desc' => __('Pinterest', 'easypress'),
        			'std' => '',
        			'class' => 'mini',
        			'type' => 'text');

		$options[] = array(	'id' => 'social_rss',
							'desc' => __('RSS Feed', 'easypress'),
        			'std' => '',
        			'class' => 'mini',
        			'type' => 'text');

		$options[] = array(	'id' => 'social_tumblr',
							'desc' => __('Tumblr', 'easypress'),
        			'std' => '',
        			'class' => 'mini',
        			'type' => 'text');

	    $options[] = array(	'id' => 'social_flickr',
								'desc' => __('Flickr', 'easypress'),
	        			'std' => '',
	        			'class' => 'mini',
	        			'type' => 'text');

	    $options[] = array(	'id' => 'social_instagram',
								'desc' => __('Instagram', 'easypress'),
	        			'std' => '',
	        			'class' => 'mini',
	        			'type' => 'text');

	    $options[] = array(	'id' => 'social_dribbble',
								'desc' => __('Dribbble', 'easypress'),
	        			'std' => '',
	        			'class' => 'mini',
	        			'type' => 'text');

	    $options[] = array(	'id' => 'social_skype',
								'desc' => __('Skype', 'easypress'),
	        			'std' => '',
	        			'class' => 'mini',
	        			'type' => 'text');

	    $options[] = array(	'id' => 'social_foursquare',
								'desc' => __('Foursquare', 'easypress'),
	        			'std' => '',
	        			'class' => 'mini',
	        			'type' => 'text');

		$options[] = array(	'id' => 'social_soundcloud',
								'desc' => __('SoundCloud', 'easypress'),
	        			'std' => '',
	        			'class' => 'mini',
	        			'type' => 'text');

		$options[] = array(	'id' => 'social_github',
						'desc' => __('Github', 'easypress'),
	        			'std' => '',
	        			'class' => 'mini',
	        			'type' => 'text');

		$options[] = array( 'name' => __('Other', 'easypress'),
							'type' => 'heading');

		$options[] = array( 'name' => __('Custom CSS', 'easypress'),
							'desc' => __('Additional CSS', 'easypress'),
							'id' => 'custom_css',
							'std' => '',
							'type' => 'textarea');

    /* Analytics theme option */
	$options[] = array( 'name' => __('Analytics', 'easypress'), 
						'type' => 'heading');

	$options[] = array( 'name' => __('Google Analytics Tracking ID', 'easypress'),
						'desc' => __('Paste the tracking id above.', 'easypress'),
						'id' => 'ga_tracking_id',
						'std' => '',
						'type' => 'text');
	/* End Analytics theme option */

	return $options;
}