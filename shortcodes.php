<?php

function easypress_blockquote_shortcode($atts, $content=null){
   
  $blockquote = '<blockquote>' . $content . '<cite>'. $atts['source'] .'</cite></blockquote>';
  
  return $blockquote;
}

function easypress_suggested_tweet_shortcode($atts, $content=null){
	return 
	    '<div>'.
	    '<h3 class="suggested-tweet-title">Share on Twitter</h3>' .
		'<a href="#" class="suggested-tweet-bubble">'.
		'<span class="suggested-tweet-text">'. $content .' by '. $atts['by'] .'</span>'.
		'<span class="suggested-tweet-hashtag"> '. $atts['hashtag']. '</span>'.
		'</a>'.
		'<p class="suggested-tweet-link">'.
		'<a href="https://twitter.com/intent/tweet?hashtags=Jekyll%2Cjekyllrb%2CTwitter&amp;related=jekyllrb%2C2john4tv%2Crichhollis&amp;text=Introducing+the+Suggested+Tweet+Plugin+for+Jekyll+by+%40DavidEnsinger&amp;url=http%3A%2F%2Fdavidensinger.com%2F2013%2F09%2Fsuggested-tweet-plugin-for-jekyll%2F" class="icon-left"><i class="fa fa-twitter"></i>Click to Tweet</a><small>(you may edit before posting.)</small></p>'.
		'</div>';
}

/*
* Shortcode for show the recent posts
*/
function easypress_recent_posts_shortcode( $atts ) {
    extract( shortcode_atts( array(
        'numbers' => '5',
    ), $atts ) );
    $rposts = new WP_Query( array( 'posts_per_page' => $numbers, 'orderby' => 'date' ) );
    if ( $rposts->have_posts() ) {
        $html = '<h3>Recent Posts</h3><ul class="recent-posts">';

        while( $rposts->have_posts() ) {
            $rposts->the_post();

            $post_thumbnail = '';

            // check if the post has a Post Thumbnail assigned to it.
            if ( has_post_thumbnail() ) {
                $mage_url = wp_get_attachment_image_src( get_post_thumbnail_id( $rposts->post->ID ), 'small' );
                $post_thumbnail = '<div class="image-content"><img src="'.$mage_url[0].'" style="width=158px height=105px"></div>';
            } 

            $excerpt = '';

            /* Disabled functionality
            $excerpt = get_the_excerpt();
            $charlength = 200;

            if ( mb_strlen( $excerpt ) > $charlength ) {
                $subex = mb_substr( $excerpt, 0, $charlength - 5 );
                $exwords = explode( ' ', $subex );
                $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
                if ( $excut < 0 ) {
                    $excerpt = mb_substr( $subex, 0, $excut );
                } else {
                    $excerpt = $subex;
                }
                $excerpt .= '...';
            } */

            $html .= sprintf(
                '<li><a href="%s" title="%s"><div class="content">'.$post_thumbnail.'<p class="title">%s</p></div></a></li>',
                get_permalink($rposts->post->ID),
                get_the_title(),
                get_the_title(),
                $excerpt
            );
        }
        $html .= '</ul>'; 
    }
    wp_reset_query();
    return $html;
}
   
add_shortcode('blockquote', 'easypress_blockquote_shortcode');
add_shortcode('suggested-tweet', 'easypress_suggested_tweet_shortcode');
add_shortcode('recent-posts', 'easypress_recent_posts_shortcode' );