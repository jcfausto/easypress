/*========= About Theme =========*/

Theme Name: Easypress
Theme URI: http://ludworks.com/wp-themes/easypress/
Version: 1.0.0
Tested up to: WP 4.0

Author: Julio Cesar Fausto
Author URI: http://jcfausto.com/
License: GNU General Public License v3.0
License URI: http://www.gnu.org/licenses/gpl.html
-------------------------------------------------------
Easypress theme, Copyright 2015 ludworks.com
Easypress WordPress theme is distributed under the terms of the GNU GPL
Easypress is based on Sparkling http://colorlib.com/wp/sparkling/, (C) 2014 Colorlib.
-------------------------------------------------------

/*========= Credits =========*/
Unless otherwise specified, all the theme files, scripts and images are licensed under GPLv2 license

Easypress theme uses:
* FontAwesome (http://fontawesome.io) licensed under the SIL OFL 1.1 (http://scripts.sil.org/OFL)
* Bootstrap (http://getbootstrap.com/) licensed under MIT license (https://github.com/twbs/bootstrap/blob/master/LICENSE)
* WP-Bootstrap-NavWalker licensed under the GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html)
* Options Framework by WP Theming licensed under the GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html)
* FlexSlider by WooThemes licensed under the GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html)
* Unless otherwise specified, all images are created by the author
/*========= Description =========*/

Easypress is based on Sparkling Theme that is a clean minimal and responsive WordPress theme well suited for travel, health, business, finance, design, portfolio, art, personal and any other creative websites and blogs. Developed using Bootstrap 3 that makes it mobile and tablets friendly. Theme comes with full-screen slider, social icon integration, author bio, popular posts widget and improved category widget. Sparkling incorporates latest web standards such as HTML5 and CSS3 and is SEO friendly thanks to its clean structure and codebase. It has dozens of Theme Options to change theme layout, colors, fonts, slider settings and much more. Theme is also translation and multilingual ready and is available in Spanish, French, Dutch, Polish, Russian, German Brazilian Portuguese and Italian. Sparkling is a free WordPress theme with premium functionality and design. Now theme is optimized to work with bbPress, Contact Form 7, Jetpack and other popular free and premium plugins.

Easypress added some features like twitter suggestions on posts, google analytics support, and custom style for blockquotes. Also added shortcodes for including twitter suggestions and blockquotes. 

/*========= Installation =========*/

You can install the theme through the WordPress installer under "Themes" > "Install themes" by searching for "Sparkling".

Alternatively you can download the file, unzip it and move the unzipped contents to the "wp-content/themes" folder of your WordPress installation. You will then be able to activate the theme.

Afterwards you can continue theme setup and customization via WordPress Dashboard - Appearance - Theme Options. For detailed theme documentation, please visit http://colorlib.com/wp/support/sparkling

/*========= Theme Features =========*/

* Bootstrap 3 integration
* Responsive design
* Unlimited color variations
* SEO friendly
* Theme Options
* Image centric approach
* Internationalized & localization
* Drop-down Menu
* Cross-browser compatibility
* Threaded Comments
* Gravatar ready
* Featured slider
* Font Awesome icons
* Custom Post Excerpt Length

/*========= Documentation =========*/

Theme documentation is available on http://ludworks.com/wp-themes/easypress/support/

/*========= Changelog =========*/

= 1.0.0 - xx.xx.2015 =

* Added suggested tweets capability on posts.
* Added google analytics support via theme options panel.
* Added custom style for blockquotes.
* Added shortcodes for insert twitter suggestions and blockquotes into posts via post editor. 
* Added custom post excerpt lenght customizable via theme options.
